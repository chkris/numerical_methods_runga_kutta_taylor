package nm.deifferentail.equations;

import java.util.ArrayList;
import java.util.List;
import nm.deifferentail.equations.methods.Analitycal;
import nm.deifferentail.equations.methods.DeMethod;
import nm.deifferentail.equations.methods.RungeKutta;
import nm.deifferentail.equations.methods.Taylor;

public class Main {
    public static void main(String args[])
    {
        List<DeMethod> methods = new ArrayList<>();
        
        methods.add(new RungeKutta());
        methods.add(new Taylor());
        methods.add(new Analitycal());

        
        for(DeMethod method : methods)
        {
            method.execute();
        }
        
        display(methods);
    }
    
    public static void display(List<DeMethod> methods)
    {     
        int i =0 ;
        double a = 0, b = 0, c = 0;
        for(double d=DeMethod.a; d < DeMethod.b; d+=DeMethod.h){
            System.out.format("xi: " +  "%.7f" + " ", d );
            for(DeMethod method : methods)
            {
                if(method instanceof RungeKutta) 
                    a = (double) method.getResultMap().values().toArray()[i];
                
                if(method instanceof Taylor) 
                    b = (double) method.getResultMap().values().toArray()[i];
                    
                if(method instanceof Analitycal) 
                    c = (double) method.getResultMap().values().toArray()[i];
                
                System.out.format("    " + method.getName() + "[yi]: " + "%.7f", method.getResultMap().values().toArray()[i]);
            }
            System.out.format("   diff[A - T]: " + "%.7f", calcAbsDiff(c, b));
            System.out.format("   diff[A - RK]: " + "%.7f", calcAbsDiff(c, a));
            System.out.format("   diff[A - T] - diff[A - RK]: " + "%.7f", calcAbsDiff(c, b) - calcAbsDiff(c, a));
            i++;
            System.out.println();
        }
    }
    
    public static double calcAbsDiff(double a, double b)
    {
        return Math.abs(a - b);
    }
}
