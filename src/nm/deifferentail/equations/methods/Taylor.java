package nm.deifferentail.equations.methods;

import java.util.HashMap;

public class Taylor extends DeMethod{

    public Taylor(){
        name = "T";
    }
    
    public double calcY(double x, double y, double h)
    {
        double f_d1 = -2*x*Math.pow(y,2);
        double f_d2 = -2*Math.pow(y, 2) - 4*x*y*f_d1;
        double f_d3 = -8*y*f_d1 - 4*x*Math.pow(f_d1, 2) - 4*x*y*Math.pow(f_d2, 2);
        double f_d4 = -12*Math.pow(f_d1, 2) - 12*y*Math.pow(f_d2, 2) - 12*x*f_d1*f_d2- 4*x*y*f_d3;
           
        return fS(x) + h*f_d1 + h*h*f_d2/2 + h*h*h*f_d3/6 + h*h*h*h*f_d4/24;
    }
    
    public void execute() {
        result = new HashMap<>();
        double yi = f_0;
        double xi = a;
        
        result.put(xi, yi);
        do{
           yi = calcY(xi, yi, h);
           
           xi += h;
           result.put(xi, yi);
        }while(xi < b);
    }
    
}
