package nm.deifferentail.equations.methods;

import java.util.HashMap;

public class Analitycal extends DeMethod{
    
    public Analitycal(){
        name = "A";
    }
    
    public void execute() {
        result = new HashMap<>();
        double xi = a;
        double yi = f_0;
        
        result.put(xi, yi);
        do{
           yi = fS(xi);
           xi += h;   
           result.put(xi, yi);
        }while(xi < b);
    }   
}
