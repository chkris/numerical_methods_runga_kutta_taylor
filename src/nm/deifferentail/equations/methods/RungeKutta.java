package nm.deifferentail.equations.methods;

import java.util.HashMap;

public class RungeKutta extends DeMethod{

    public RungeKutta(){
        name = "RK";
    }
    
    public double calculateWi(double wi, double ti, double h)
    {
        double k1 = h * f(ti, wi);
        double k2 = h * f(ti + h/2, wi + k1/2);
        double k3 = h * f(ti + h/2, wi + k2/2);
        double k4 = h * f(ti + h, wi + k3);
        
        return wi + (k1 + 2 * k2 + 2 * k3 + k4)/6;
    }
    
    public void execute()
    {
        result = new HashMap<>();
        double ti = a;
        double wi = f_0;
        
        result.put(ti, wi);
        do{
           wi = calculateWi(wi, ti, h);
           ti += h;   
           result.put(ti, wi); 
        }while(ti < b);
    }
}
