package nm.deifferentail.equations.methods;

import java.util.HashMap;
import java.util.Map;
import static nm.deifferentail.equations.methods.DeMethod.a;


public abstract class DeMethod {
    
    public static double a = 0;
    public static double b = 2;
    public static double h = 0.01;
    public double f_0 = fS(a);
    public String name;
    
    Map<Double, Double> result;
        
    public double fS(double t) 
    {
        return 1/(1+Math.pow(t, 2));
    }
            
    public double f(double t, double y)
    {
        return -2*t*Math.pow(y, 2);
    }
    
    public abstract void execute();
    
    public Map<Double, Double> getResultMap()
    {
        return result;
    }
    
    public String getName()
    {
        return name;
    }
}